class Weather < Formula
    desc "A command line tool to retrieve local weather"
      homepage "https://bitbucket.org/sergsoares/weather"
        url "https://bitbucket.org/sergsoares/weather/raw/9866e6578eed748ffdf1197d458ebdbb217e684b/archive/weather-1.0.0.tar.gz"
          sha256 "efffeb5f01261f070bad5e581fc8929d55db7c8784309ed5cafe72a7755e931e"
            version "1.0.0"

              depends_on "curl"

                bottle :unneeded

                  def install
                        bin.install "weather"
                          end
end
